#! /usr/bin/python3
from ldap3 import *
import re
import argparse
import json
import libknot.control



# Parse arguments
parser = argparse.ArgumentParser(description='Generation of DNS zones.')
parser.add_argument('--host', type=str, help='LDAP host')
parser.add_argument('--port', type=int, help='LDAP port')
parser.add_argument('--binddn', type=str, help='LDAP bind DN')
parser.add_argument('--bindpw', type=str, help='LDAP bind password')
parser.add_argument('--basedn', type=str, help='DNS base DN')
parser.add_argument('--zone', type=str, help='zone name')
parser.add_argument('--autoaccept', type=bool, help='Accept automatically', default=False)
parser.add_argument('--config', type=str, help='path to a config file', default="")
parser.add_argument('--flush_before_add', type=bool, help='Flush the zone before adding elements', default=True)

args = parser.parse_args()



if args.config != "":
    with open(args.config) as file:
        config = json.load(file)
        ldap_host = config['ldap_host']
        ldap_port = config['ldap_port']
        binddn = config['binddn']
        bindpw = config['bindpw']
        dns_base = config['dns_base']
        zone_name = config['zone_name']
        auto_accept = config['autoaccept']

if args.host:
    ldap_host = args.host
if args.port:
    ldap_port = args.port
if args.binddn:
    binddn = args.binddn
if args.bindpw:
    bindpw = args.bindpw
if args.basedn:
    basedn = args.basedn
if args.zone:
    zone_name = args.zone
if args.autoaccept:
    auto_accept = args.autoaccept
if args.flush_before_add:
    flush_before_add = args.flush_before_add

first_dns_base = ""
for dc in zone_name.split('.'):
    if dc != "":
        first_dns_base = f"{first_dns_base},dc={dc}"
first_dns_base = re.sub(r'^,', '', f"{first_dns_base},{dns_base}")



def build_record(record_name, record_type, record_value, ctl):
    if record_type == "aRecord":
        for ip in record_value:
            if re.match ('^([0-9]+\.){3}[0-9]+$', ip):
                ctl.send_block(cmd="zone-set", ttl="3600", zone=zone_name, owner=record_name, rtype="A", data=ip)
            else:
                ctl.send_block(cmd="zone-set", ttl="3600", zone=zone_name, owner=record_name, rtype="AAAA", data=ip)
    elif record_type == "cNAMERecord":
        ctl.send_block(cmd="zone-set", ttl="3600", zone=zone_name, owner=record_name, rtype="CNAME", data=record_value[0])
    elif record_type == "mXRecord":
        for mx in record_value:
            ctl.send_block(cmd="zone-set", ttl="3600", zone=zone_name, owner=record_name, rtype="MX", data=mx)
    elif record_type == "nSRecord":
        for ns in record_value:
            ctl.send_block(cmd="zone-set", ttl="3600", zone=zone_name, owner=record_name, rtype="NS", data=ns)
    elif record_type == "description":
        for descr in record_value:
            if re.match('^PTR::([a-zA-Z0-9\-_]+\.?)+:([a-zA-Z0-9\-_]+\.?)+$', descr):
                ctl.send_block(cmd="zone-set", ttl="3600", zone=zone_name, owner=f"{descr.split(':')[2]}.{record_name}", rtype="PTR", data=f"{descr.split(':')[3]}")
            elif re.match('^TLSA:[0-9]+:(tcp|udp):[0-3]:[0-1]:[0-2]:[a-zA-Z0-9]+$', descr):
                s = descr.split(':')
                ctl.send_block(cmd="zone-set", ttl="3600", zone=zone_name, owner=f"_{s[1]}._{s[2]}.{record_name}", rtype="TLSA", data=f"{s[3]} {s[4]} {s[5]} {s[6]}")
            else:
                ctl.send_block(cmd="zone-set", ttl="3600", zone=zone_name, owner=record_name, rtype="TXT", data=descr)
    else:
        print (f"Unknown record type: {record_type}")

def record_name_of_dn(dn):
    global dns_base
    return re.sub(r"dc=([^,]+),", r"\1.", dn.replace(f"{dns_base}", ''))





server = Server (ldap_host, port=int(ldap_port), get_info=ALL)
conn = Connection (server, user=binddn, password=bindpw)
conn.bind()
if conn.result['result'] == 0:
    print("Connection to the ldap established.")
else:
    print(f"Could not connect to the LDAP: {conn.result['description']}")
    print(server, binddn, bindpw)
    exit(-1)

ctl = libknot.control.KnotCtl()
try:
    ctl.connect("/var/run/knot/knot.sock")
    ctl.set_timeout(3600)
    print("Connection to knot established.")

    # Begin the transaction
    ctl.send_block(cmd="zone-begin", zone=zone_name)
    resp = ctl.receive_block()
    if resp != {} :
        print(f"Could not start the transaction: {resp}")
        exit(-1)



    # Clear the zone if requested (default)
    if flush_before_add:
        print("Flushing the zone")
        ctl.send_block(cmd="zone-read", zone=zone_name)
        zone = ctl.receive_block()
        for sub in zone[zone_name]:
            for tip in zone[zone_name][sub]:
                if tip not in ["RRSIG", "NSEC3", "NSEC3PARAM", "DNSKEY", "SOA"]:
                    ctl.send_block(cmd="zone-unset", zone=zone_name, owner=sub, rtype=tip)
                    resp = ctl.receive_block()



    # Generate the new zone

    print("Generating a new zone")
    conn.search(search_base = first_dns_base,
        search_filter='(objectClass=dNSDomain)',
        search_scope=SUBTREE,
        attributes=['*'])
    for a in conn.response:
        record_name = record_name_of_dn(a['dn'])
        for k in a['attributes']:
            if k in ['dc', 'objectClass']:
                continue
            build_record(record_name, k, a['attributes'][k], ctl)



    # Check the diff with the original zone
    #ctl.send_block(cmd="zone-diff", zone=zone_name)
    #diff = ctl.receive_block()
    #if diff == {}:
    #    print("\n\tAucune modification à faire: aborting.\n")
    #    #ctl.send_block(cmd="zone-abort", zone=zone_name)
    #    raise SystemExit
    #for a in diff[zone_name]:
    #    print(f"Modification: {a}")



    # There is something to do, let the user decide.
    def commit_changes ():
        ctl.send_block(cmd="zone-commit", zone=zone_name)
        print(f"Modification of {zone_name} committed.")
    def abort_changes ():
        ctl.send_block(cmd="zone-abort", zone=zone_name)
        print(f"Modification of {zone_name} aborted (return: {out.stdout.decode()}).")

    #if auto_accept:
    #    commit_changes()
    #elif input("\n\tDo you agree with the modifications to brong the zone ? [Y/n] ") in [ "n", "N" ]:
    #    abort_changes()
    #else:
    #    commit_changes()
    commit_changes()

except libknot.control.KnotCtlError as exc:
    print (exc)
finally:
    print("closing connections to the LDAP and knot")
    ctl.send(libknot.control.KnotCtlType.END)
    ctl.close()
    conn.unbind()

