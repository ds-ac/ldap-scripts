#! /usr/bin/python

from ldap3 import *
import re
import argparse
import json
import functools

parser = argparse.ArgumentParser(description='Add a contact to the LDAP')
parser.add_argument('--config', type=str, help='Path to the configuration path', default='config.json')

args = parser.parse_args()

with open(args.config) as file:
    config = json.load(file)

server = Server (config['host'], port=config['port'], get_info=ALL)
conn = Connection (server, user=config['binddn'], password=config['bindpw'])
conn.bind()




def cn_of_entries (entry, suffix):
    return [re.sub (r"cn=([a-zA-Z0-9\-\.]+),", r"\1", re.sub(suffix, '', e)) for e in entry]

def gen_person (dic):
    if "mail" not in dic or "sn" not in dic:
        print ( "Error." )
        return None
    email=','.join([f"<{m}>" for m in dic['mail']])
    return f"alias {dic['cn'][0]} {dic['sn'][0]} {email}"

def gen_alias (config, dic):
    if "member" not in dic or 'cn' not in dic:
        print ( "Error." )
        return None
    a = ','.join(cn_of_entries(dic['member'],f"{config['contacts_prefix']},{config['basedn']}"))
    return f"alias {dic['cn'][0]} {a}"

def idtransfo (e):
    return e

def addrTransfo (a):
    return re.sub(r'\n', r"\n\t", re.sub(r'\$', r"\n", a))

def repeatprint (name, arr, transfo = idtransfo):
    print(f"{name}:")
    for elem in arr:
        print(f"\t{transfo(elem)}")
    print()

printableuserfields = [
    {
        'inldap': 'sn',
        'toprint': 'Name'
    },
    {
        'inldap': 'mail',
        'toprint': 'Email Address'
    },
    {
        'inldap': 'homePostalAddress',
        'toprint': 'Postal Address',
        'transfo': addrTransfo
    },
    {
        'inldap': 'mobile',
        'toprint': 'Mobile phone number'
    },
    {
        'inldap': 'preferredLanguage',
        'toprint': 'Nationnalité'
    },
    {
        'inldap': 'initials',
        'toprint': 'Initials'
    },
]

def search_user (conn, name):
    conn.search(f"{config['contacts_prefix']},{config['basedn']}", search_filter=f"(cn={name})", attributes=['*'])
    if conn.result['result'] != 0 or 'inetOrgPerson' not in conn.response[0]['attributes']['objectClass']:
        print ("Could not find the user {name}.")
        conn.unbind()
        exit(-1)
    user_dn = conn.response[0]['dn']
    attrs = conn.response[0]['attributes']
    for f in printableuserfields:
        if f['inldap'] in attrs:
            if 'transfo' in f:
                repeatprint(f['toprint'], attrs[f['inldap']], transfo=f['transfo'])
            else:
                repeatprint(f['toprint'], attrs[f['inldap']])

    if 'description' in attrs:
        for d in attrs['description']:
            if re.match (r"birthday:[0-9]{4}\.[0-9]{2}\.[0-9]{2}", d):
                s = d.split(':')
                print (f"{s[0]}\n\t{s[1]}")

def getuser(conn, config, name=""):
    if name == "":
        conn.search(f"{config['contacts_prefix']},{config['basedn']}",
            attributes=['cn'], search_filter=f"(objectClass=inetOrgPerson)")
        for r in conn.response:
            print("\n\n******************************\n\n")
            search_user(conn, r['attributes']['cn'][0])
    else:
        search_user(conn, name)

def addcontact(conn, config):
    name = input ("name of the alias: ")
    mail = input ("mail of the mail: ")
    mobile = input ("mobile phone: ")
    nationality = input ("nationality: ")
    initials = input("initials: ")
    address = input ("address: ")
    birthday = input ("birthday: ")
    sn = input ("name of the contact: ")
    dic = {}
    if mail:
        dic['mail'] = mail
    if sn:
        dic['sn'] = sn
    if birthday:
        dic['description'] = f"birthday: {birthday}"
    if address:
        dic['homePostalAddress'] = address
    if initials:
        dic['initials'] = initials
    if nationality:
        dic['preferredLanguage'] = nationality
    if mobile:
        dic['mobile'] = mobile
    conn.add (f"cn={name},{config['contacts_prefix']},{config['basedn']}", 'inetOrgPerson', dic)
    print(conn.result)

def genalias(conn, config, output="-"):
    conn.search(search_base=f"{config['contacts_prefix']},{config['basedn']}",
        search_filter='(|(objectClass=inetOrgPerson)(objectClass=groupOfNames))',
        search_scope=SUBTREE,
        attributes=['*'])
    aliases = []
    for l in conn.response:
        if "inetOrgPerson" in l['attributes']['objectClass']:
            aliases.append (gen_person(l['attributes']))
        elif "groupOfNames" in l['attributes']['objectClass']:
            aliases.append (gen_alias(config, l['attributes']))
        else:
            print ("Error: not a valid person object.", l['attributes'])
    if output == '-':
        for a in aliases:
            print(a)
    else:
        print(f"Saving to {output}")
        with open(output, "w") as file:
            file.write("# File genrated by [aliasgen.py].\n\n")
            file.writelines([f"{line}\n" for line in aliases])

while True:
    l = input("> ")
    s = l.split()
    if s[0] in ["quit", "exit"] :
        break
    elif s[0] == "addcontact" :
        addcontact(conn, config)
    elif s[0] == "genalias" :
        if len(s) == 2:
            genalias(conn, config, s[1])
        else:
            genalias(conn, config)
    elif s[0] == "getuser" :
        if len(s) == 2:
            getuser(conn, config, s[1])
        else:
            getuser(conn, config)
    else:
        print(f"Unknown command: {l}")


conn.unbind()

